characterInput= input("Enter a character [O, X, Y, Z] : ")

column= int(input ("Enter a non-negative odd integer (3, 5, 7, etc.): "))



# Number of character width in each line

width = (2 * column) - 1

# Function to find the absolute value
# of a number D

def abs(d):
	if d < 0:
		return -1*d
	else:
		return d


def printO() :
	space = column // 3
	width = column // 2 + column // 5 + space + space
	for i in range(0,column) :
		for j in range(0,width + 1) :
			if ( j == width - abs(space) or j == abs(space)):
				print("*",end="")
			elif( (i == 0 or i == column - 1) and j > abs(space) and j < width - abs(space) ) :
				print("*",end="")
			else :
				print(end=" ")

		if( space != 0 and i < column // 2) :
			space = space -1
		elif ( i >= (column // 2 + column // 5) ) :
			space = space -1

		print()


# Function to print the pattern of 'X'
def printX() :
	counter = 0
	for i in range(0,columnt+1) :
		for j in range(0,column+1) :
			if ( j == counter or j == column - counter ):
				print("*",end="")
			else :
				print(end=" ")
		counter = counter + 1
		print()

# Function to print the pattern of 'Y'
def printY() :
	counter = 0
	for i in range(0,column) :
		for j in range(0,column+1) :
			if ( j == counter or j == column - counter and i <= column // 2 ):
				print("*",end="")
			else :
				print(end=" ")
		print()
		if (i < column // 2) :
			counter = counter + 1

# Function to print the pattern of 'Z'
def printZ() :
	counter = column - 1
	for i in range(0,column) :
		for j in range(0,column) :
			if ( i == 0 or i == column - 1 or j == counter ):
				print("*",end="")
			else :
				print(end=" ")
		counter = counter - 1
		print()


# Function print the pattern of the
# alphabets from A to Z

def printPattern(character) :
	
	if character == 'O' : return printO()
	elif character == 'X': return printX(),
	elif character == 'Y': return printY()
	else : printZ()

# Driver Code
if __name__ == "__main__":
	character = characterInput
	printPattern(character)

# This code is contributed by rakeshsahni


